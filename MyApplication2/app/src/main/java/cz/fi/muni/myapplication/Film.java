package cz.fi.muni.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by expres on 2/3/2016.
 */
public class Film implements Parcelable {

    private int mCoverId;
    private long mReleaseDate;
    private String mCoverPath;
    private String mTitle;

    public Film() {
    }

    public Film(int coverId, long releaseDate, String coverPath, String title) {
        mCoverId = coverId;
        mReleaseDate = releaseDate;
        mCoverPath = coverPath;
        mTitle = title;
    }

    public int getCoverId() {
        return mCoverId;
    }

    public void setCoverId(int coverId) {
        mCoverId = coverId;
    }

    public long getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(long releaseDate) {
        mReleaseDate = releaseDate;
    }

    public String getCoverPath() {
        return mCoverPath;
    }

    public void setCoverPath(String coverPath) {
        mCoverPath = coverPath;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Film(Parcel in) {
        mCoverId = in.readInt();
        mReleaseDate = in.readLong();
        mCoverPath = in.readString();
        mTitle = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mCoverId);
        dest.writeLong(mReleaseDate);
        dest.writeString(mCoverPath);
        dest.writeString(mTitle);
    }

    public static final Parcelable.Creator<Film> CREATOR = new Parcelable.Creator<Film>() {
        @Override
        public Film createFromParcel(Parcel source) {
            return new Film(source);
        }

        @Override
        public Film[] newArray(int size) {
            return new Film[size];
        }
    };
}
