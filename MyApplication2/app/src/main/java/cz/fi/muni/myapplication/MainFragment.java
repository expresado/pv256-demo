package cz.fi.muni.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by expres on 2/3/2016.
 */
public class MainFragment extends Fragment{

    private OnFilmPickListener mListener;
    private Context mContext;

    public interface OnFilmPickListener {
        void onFilmPick(Film film);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFilmPickListener) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        fillListView(view);

        return view;
    }

    private void fillListView(View rootView) {
        // get data
        ArrayList<Film> filmList = FilmData.get(mContext).getFilmList();

        ListView movieGridView = (ListView) rootView.findViewById(R.id.listview_forecast);

        if (filmList != null && !filmList.isEmpty()) {
            setAdapter(movieGridView, filmList);
        }
    }


    private void setAdapter(ListView filmLV, final ArrayList<Film> filmList) {
        FilmAdapter adapter = new FilmAdapter(filmList, mContext);
        filmLV.setAdapter(adapter);

        // set on click listener
        filmLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mListener.onFilmPick(filmList.get(position));
            }
        });

        // set on long click listener
        filmLV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(mContext, filmList.get(position).getTitle(), Toast.LENGTH_SHORT)
                        .show();
                return true;
            }
        });
    }
}
