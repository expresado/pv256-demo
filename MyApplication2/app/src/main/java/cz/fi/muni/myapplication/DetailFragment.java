package cz.fi.muni.myapplication;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by expres on 2/3/2016.
 */

public class DetailFragment extends Fragment {

    public static final String ARGS_FILM = "args_film";

    private Context mContext;
    private Film mFilm;

    public static DetailFragment newInstance(Film film) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARGS_FILM, film);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        if(getArguments()!= null) {
            mFilm = getArguments().getParcelable(ARGS_FILM);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        TextView titleTv = (TextView) view.findViewById(R.id.detail_forecast_textview);
        TextView titleLowTv = (TextView) view.findViewById(R.id.detail_forecast_textview_low);
        ImageView coverIv = (ImageView) view.findViewById(R.id.detail_icon);
        if(mFilm!=null){
        titleTv.setText(mFilm.getTitle());
            titleLowTv.setText(mFilm.getCoverPath());
        setCoverImage(coverIv, mFilm);}

        return view;
    }

    private void setCoverImage(ImageView coverIv, Film film) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            coverIv.setImageDrawable(mContext.getDrawable(film.getCoverId()));
        } else {
            coverIv.setImageDrawable(mContext.getResources().getDrawable(film.getCoverId()));
        }
    }
}