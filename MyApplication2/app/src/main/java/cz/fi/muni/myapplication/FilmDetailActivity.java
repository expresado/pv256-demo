package cz.fi.muni.myapplication;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
/**
 * Created by expres on 2/3/2016.
 */

public class FilmDetailActivity extends AppCompatActivity {

    public static final String EXTRA_FILM = "extra_film";

    private Film mFilm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if(savedInstanceState== null){
            mFilm = getIntent().getParcelableExtra(EXTRA_FILM);
            FragmentManager fm = getSupportFragmentManager();
            DetailFragment fragment = (DetailFragment) fm.findFragmentById(R.id.weather_detail_container);

            if (fragment == null) {
                fragment = DetailFragment.newInstance(mFilm);
                fm.beginTransaction().add(R.id.weather_detail_container, fragment).commit();
            }
        }
    }

}

