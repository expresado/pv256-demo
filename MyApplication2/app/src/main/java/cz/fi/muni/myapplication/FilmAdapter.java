package cz.fi.muni.myapplication;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by expres on 2/3/2016.
 */
public class FilmAdapter extends BaseAdapter{
    private static final String TAG = "FilmAdapter";

    private Context mAppContext;
    private ArrayList<Film> mFilmList;

    public FilmAdapter(ArrayList<Film> filmList, Context context) {
        mFilmList = filmList;
        mAppContext = context.getApplicationContext();
    }

    @Override
    public int getCount() {
        return mFilmList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilmList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            Log.i(TAG, "inflate radku " + position);

            convertView = LayoutInflater.from(mAppContext).inflate(R.layout.list_item_forecast, parent, false);
            ViewHolder holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        Log.i(TAG, "recyklace radku " + position);

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.bindView(mFilmList.get(position));

        return convertView;
    }


    private class ViewHolder {
        private ImageView coverIv;

        public ViewHolder(View view) {
            coverIv = (ImageView) view.findViewById(R.id.list_item_icon);
        }

        public void bindView(Film film) {
            if (film == null)  return;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                coverIv.setImageDrawable(mAppContext.getDrawable(film.getCoverId()));
            } else {
                coverIv.setImageDrawable(mAppContext.getResources().getDrawable(film.getCoverId()));
            }
            coverIv.setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }
}
